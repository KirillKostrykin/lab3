from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^home/$', views.home, name='home'),
	url(r'^description/$', views.description, name='description'),
	url(r'^trashes/$', views.trashes, name='trashes'),
	url(r'^trashes/new/$', views.create_trash, name='create_trash'),
	url(r'^trashes/trash_info/(?P<id>\d+?)$', views.trash_info, name='trash_info'),
	url(r'^trashes/update/(?P<id>\d+?)$', views.trash_update, name='trash_update'),
	url(r'^trashes/delete/(?P<id>\d+?)$', views.trash_delete, name='trash_delete'),
	url(r'^trashes/tasks/(?P<id>\d+?)$', views.trash_tasks, name='trash_tasks'),
	url(r'^tasks/$', views.tasks, name='tasks'),
	url(r'^tasks/new/$', views.create_task, name='create_task')
]