# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib import messages

import myrm.trash
import myrm.deleter
import myrm.logger
import myrm.errors
from logging import CRITICAL, WARNING

import shutil
import os

from myrm.config import DEFAULT_TRASH_PATH, DEFAULT_MAX_FILE_SIZE, DEFAULT_MAX_FILE_TIME

class Trash(models.Model):
	"""docstring for model Trash"""
	destination = models.CharField(max_length=256, default='/home/kirill/newtrash')
	name = models.CharField(max_length=50, default='defaultTrash')
	max_file_size = models.IntegerField(default=DEFAULT_MAX_FILE_SIZE)
	max_file_time = models.IntegerField(default=DEFAULT_MAX_FILE_TIME)

	def __str__(self):
		return self.name

class Task(models.Model):
	"""docstring for model Task"""
	name = models.CharField(max_length=50)
	trashbin = models.ForeignKey(Trash, related_name="tasks", on_delete=models.CASCADE)
	path = models.CharField(max_length=256, help_text="Please, enter absolute path")
	action = models.CharField(max_length=127)
	flags = models.CharField(max_length=20, blank=True)
	regex = models.CharField(max_length=256, blank=True)

	def __str__(self):
		return "{} {}".format(self.name, self.action)
