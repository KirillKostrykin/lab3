# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from app.models import Trash, Task
import os
import shutil

class AnimalTestCase(TestCase):
	TEST_DIR = 'TEST_DIR'
	PATH_OF_EMPTY_DIR = os.path.join(TEST_DIR, 'EMPTY_DIR')
	PATH_OF_NOT_EMPTY_DIR = os.path.join(TEST_DIR, 'NOT_EMPTY_DIR')
	PATH_OF_FILE = os.path.join(TEST_DIR, 'FILE')

	def setUp(self):
		# os.makedirs(self.TEST_DIR)
		# os.makedirs(self.PATH_OF_EMPTY_DIR)
		# os.makedirs(os.path.join(self.PATH_OF_NOT_EMPTY_DIR, 'DIR1', 'DIR2'))
		# open(self.PATH_OF_FILE, 'wb')
		Trash.objects.create(name="trash1", destination="/home/krupchic/trash")
		Trash.objects.create(name="trash2", destination="/home/krupchic/trash")

	def test_objects_creation(self):
		trash1 = Trash.objects.get(name="trash1")
		trash2 = Trash.objects.get(name="trash2")
		self.assertNotEqual(trash1, trash2)
		self.assertEqual(str(trash1), 'trash1')

	def tearDown(self):
		if os.path.exists(self.TEST_DIR):
			shutil.rmtree(self.TEST_DIR)