# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages
import datetime

from forms import TaskForm, TrashForm
from models import Task, Trash

import myrm.trash

import os
import shutil
from tasks import remove as rem
from tasks import restore as rest

def home(request):
	return render(request, 'home.html', {})

def description(request):
	return render(request, 'description.html', {})

def create_trash(request):
	token = { 'form': TrashForm(), }
	return render(request, 'create_trash.html', token)

def trashes(request):
	if request.method == "POST":
		form = TrashForm(request.POST)
		if form.is_valid():
			trash = form.save()
			if not os.path.exists(trash.destination):
				new_trash = myrm.trash.Trash(trash_path=trash.destination, max_file_size=trash.max_file_size, max_file_time=trash.max_file_time)
				new_trash.create_trash(trash.destination)
				trash.save()				
				token = {
					"form": form, 
					"trashes_": Trash.objects.all(),
				}
				return render(request, "trashes.html", token)
			else:
				trash.delete()
				messages.add_message(request, messages.ERROR, "This path is already exists")
				form = TrashForm()
				token = {
					"form": form
				}
				return render(request, "create_trash.html", token)
		else:
			return Http404()
	else:
		form = TrashForm()
		token = {
			'form': form,
			'trashes_': Trash.objects.all(),
		}
		return render(request, 'trashes.html', token)

def trash_info(request, id):
	token = {
		'form': TrashForm(instance=Trash.objects.get(id=id)),
		'id': id,
	}
	return render(request, 'trash_info.html', token)
	
def create_task(request):
	token = { 'form': TaskForm(), }
	return render(request, 'create_task.html', token)

def tasks(request):
	if request.method == "GET":
		form = TaskForm()
		token = {
			"form": form,
			"tasks": Task.objects.all(),
		}
		return render(request, "tasks.html", token)
	else:
		form = TaskForm(request.POST)
		if form.is_valid():

			task = form.save()
			
			if task.action == "remove":
				(message_type, message) = rem(task.path, task.regex, task.trashbin.destination, flags=task.flags)
				messages.add_message(request, message_type, message)
				if 30 == message_type:
					task.delete()
			
			if task.action == "restore":
				(message_type, message) = rest(task)
				messages.add_message(request, message_type, message)
			
			return render(request, "tasks.html", {"tasks": Task.objects.all()})
		
		token = {
			"form": form,
			"tasks": Task.objects.all(),
		}
		return render(request, "create_task.html", token)

def trash_update(request, id):
	if request.method == "POST":
		trash = Trash.objects.get(id=id)
		old_path = os.path.abspath(trash.destination)
		form = TrashForm(request.POST, instance=Trash.objects.get(id=id))
		if form.is_valid():
			trash = form.save()
			new_path = os.path.abspath(trash.destination)
			if new_path != old_path:
				os.renames(old_path, new_path)
			
			return redirect("trashes")

def trash_delete(request, id):
	if request.method == "GET":
		trash = Trash.objects.get(id=id)
		if os.path.exists(trash.destination):
			shutil.rmtree(trash.destination)
		trash.delete()
		return redirect("trashes")

def trash_tasks(request, id):
	token = {
		"tasks": Trash.objects.get(id=id).tasks.all(),
	}
	return render(request, "tasks.html", token)

	#for flag in Task.object.get(id=id).flags:
