from django import forms
from models import Task, Trash

ACTIONS = (("remove", "Remove",), ("restore", "Restore",))
FLAGS = (("dir", "Turn on directory removing",), ("recursive", "Turn on recursive removing",), ("regex", "Use removing by regex"))

class TrashForm(forms.ModelForm):
	"""docstring for TrashForm"""
	class Meta:
		model = Trash
		fields = ["destination", "name", "max_file_size", "max_file_time"]
		labels = {"max_file_size": ("Max file size"),} 
		labels = {"max_file_time": ("Max file time"),}

class TaskForm(forms.ModelForm):
	"""docstring for TaskForm"""
	action = forms.ChoiceField(widget=forms.RadioSelect, choices=ACTIONS)
	flags = forms.MultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple, choices=FLAGS)
	class Meta:
		model = Task
		fields = ["name", "action", "trashbin", "path", "flags", "regex"]