import myrm.trash
import myrm.deleter
import myrm.logger
import myrm.errors
from myrm.config import DEFAULT_TRASH_PATH, DEFAULT_MAX_TRASH_SIZE, DEFAULT_MAX_FILE_SIZE, DEFAULT_MAX_FILE_TIME
from logging import CRITICAL, WARNING

from django.contrib import messages
import os
from models import Task, Trash

import threading
import re

def tree_round(path, regex, trash_destination):
	sub_threads = []

	for item in os.listdir(path):
		print regex

		item_path = os.path.join(path, item)
		if re.search(regex, item_path) is not None:
			remove(item_path, regex, trash_destination)
			print item_path
			#os.remove(item_path)
		if os.path.isdir(item_path):
			t = threading.Thread(target=tree_round, args=(item_path, regex, trash_destination, ))      
			sub_threads.append(t)
			t.start()
	
	for thread in sub_threads:
		thread.join()


def remove(path, regex, trash_destination, flags="recursive"):
	empty_dir, recursive = False, False

	if "dir" in flags:
		empty_dir = True
	if "recursive" in flags:
		recursive = True 
	if "regex" in flags:
		tree_round(path, regex, trash_destination)
		return (messages.INFO, "Everything is ok")

	trash = Trash.objects.get(destination=trash_destination)

	my_logger = myrm.logger.Logger(CRITICAL)

	trasher = myrm.trash.Trash(
		max_file_time=trash.max_file_time, 
		max_file_size=trash.max_file_size,
		trash_path=trash_destination,
		recursive=recursive,
		empty_dir=empty_dir,
		force=True,
		totrash=True,
		logger_=my_logger) 

	my_deleter = myrm.deleter.Deleter(
		recursive=recursive,
		empty_dir=empty_dir,
		force=True,
		totrash=True,
		logger_=my_logger)

	message = (messages.INFO, "File {} was moved to Trash ".format(path))

	if not os.path.exists(path):
		message = (messages.WARNING, "Cannot remove {}. Such file is not found".format(path))
		return message

	if os.path.isdir(path):
		if not my_deleter.empty_dir and not my_deleter.recursive:
			message = (messages.WARNING, "Cannot remove {}. It's a directory".format(path))
			return message

		listdir = os.listdir(path)

		if len(listdir) > 0 and not my_deleter.recursive:
			message = (messages.WARNING,"Cannot remove {}. Directory not empty".format(path))
			return message

	trasher.move_to_trash(path)

	trasher.check_politics(my_deleter)

	return message

def restore(task):
	empty_dir, recursive, regex = False, False, False

	if 'dir' in task.flags:
		empty_dir = True
	if 'recursive' in task.flags:
		recusive = True
	if 'regex' in task.flags:
		regex = True 	

	trash = Trash.objects.get(destination=task.trashbin.destination)
	
	my_logger = myrm.logger.Logger(CRITICAL)

	trasher = myrm.trash.Trash(
		max_file_time=trash.max_file_time, 
		max_file_size=trash.max_file_size,
		trash_path=task.trashbin.destination,
		recursive=recursive,
		empty_dir=empty_dir,
		force=True,
		totrash=True,
		logger_=my_logger) 

	my_deleter = myrm.deleter.Deleter(
		recursive=recursive,
		empty_dir=empty_dir,
		force=True,
		totrash=True,
		logger_=my_logger
		)

	trasher.restore_file_with_path(task.path)
	message = (messages.INFO,"Task create")
	return message